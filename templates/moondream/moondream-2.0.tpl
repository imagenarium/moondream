<@requirement.NODE ref='moondream' primary='moondream-${namespace}' single='false' />

<@requirement.PARAM name='API_PORT' required='false' type='port' scope='global' />

<@img.TASK 'moondream-${namespace}' 'imagenarium/moondream:2.0-cpu'>
  <@img.NODE_REF 'moondream' />
  <@img.VOLUME '/root/.cache' />
  <@img.PORT PARAMS.API_PORT '80' />
  <@img.CHECK_PORT '80' />
</@img.TASK>
