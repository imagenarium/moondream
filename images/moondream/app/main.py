import uvicorn
import os
import io
from transformers import AutoModelForCausalLM, AutoTokenizer
from PIL import Image
from fastapi import FastAPI, UploadFile, HTTPException
import logging
from contextlib import asynccontextmanager

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

@asynccontextmanager
async def lifespan(app: FastAPI):
    # Startup event
    try:
        model_id = "vikhyatk/moondream2"
        revision = "2024-08-26"
        app.model = AutoModelForCausalLM.from_pretrained(model_id, trust_remote_code=True, revision=revision)
        app.tokenizer = AutoTokenizer.from_pretrained(model_id, revision=revision)
        logger.info("Model and tokenizer initialized successfully.")
    except Exception as e:
        logger.error(f"Error initializing model and tokenizer: {str(e)}")
        raise

    yield  # This is where the application runs

    # Shutdown event
    try:
        del app.model
        del app.tokenizer
        logger.info("Model and tokenizer cleaned up successfully.")
    except Exception as e:
        logger.error(f"Error cleaning up model and tokenizer: {str(e)}")

app = FastAPI(lifespan=lifespan)

@app.post("/generate", name="Сгенерировать текст по изображению", tags=["AI Vision"])
async def caption(file: UploadFile, prompt: str = "Describe this image."):
    try:
        # Read and convert the uploaded image
        contents = await file.read()
        image = Image.open(io.BytesIO(contents)).convert('RGB')

        # Encode the image (assuming there's a method to encode images for your model)
        enc_image = app.model.encode_image(image)

        # Generate a caption using the model
        generated_text = app.model.answer_question(enc_image, prompt, app.tokenizer)

        return {"caption": generated_text}

    except Exception as e:
        logger.error(f"Error processing file: {str(e)}")
        raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")

    finally:
        # Ensure the file is closed after processing
        await file.close()

if __name__ == "__main__":
    port = int(os.getenv("PORT", 80))
    host = os.getenv("HOST", "0.0.0.0")
    workers = int(os.getenv("WORKERS", os.cpu_count()))

    uvicorn.run("main:app", host=host, port=port, workers=workers)
